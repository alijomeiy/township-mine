namespace TownshipMineSpecific.Scripts.Models
{
    public class PreciousMap : Map
    {
        public PreciousMap(string id, int width, int height,
            float emptynessRatio, Content[] notEmptyAcceptable, int order) :
            base(id, width, height, emptynessRatio, notEmptyAcceptable, order)
        {
        }
        
        public override void FillRandom()
        {
            FillMapWithAcceptableContents();
            MakeSomeTileEmpty(1, Contents.Count - 1);
        }

        protected override void AddRandomRowBottom()
        {
            base.AddRandomRowBottom();
            MakeSomeTileEmpty(0, 0);
        }

        public void AddNewRow()
        {
            AddRandomRowBottom();
        }
    }
}