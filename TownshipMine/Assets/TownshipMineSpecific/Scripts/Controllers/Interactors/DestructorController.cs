using System;
using System.Collections.Generic;
using TownshipMineSpecific.Scripts.Models;
using TownshipMineSpecific.Scripts.Views;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace TownshipMineSpecific.Scripts.Controllers.Interactors
{
    public abstract class DestructorController : InteractorController
    {
        [SerializeField] private string destructorID;
        [SerializeField] private DestructorView view;
        private List<DestructibleMapController> _destructibleMaps;
        protected bool IsSelected { get; set; }

        public static Action<List<Vector2Int>> Destructed;

        private void Awake()
        {
            ControlDestructibleMapNull();
        }

        protected override void Interact(TileBase tile, Vector3Int position)
        {
            var pos = new Vector2Int(position.x, position.y);
            foreach (var mapController in _destructibleMaps)
            {
                TryDestruct(mapController.Map, pos);
            }
        }

        protected override bool IsInteractable(string mapID, TileBase tile,
            Vector3Int pos)
        {
            var baseResult = base.IsInteractable(mapID, tile, pos);
            return baseResult && IsSelected;
        }

        protected override void UnsubscribeEvents()
        {
            DestructorView.SelectionStateChanged -= HandleSelecting;
        }

        protected override void SubscribeEvents()
        {
            DestructorView.SelectionStateChanged += HandleSelecting;
        }

        private void HandleSelecting(string selectedDestructorID, bool newState)
        {
            if (selectedDestructorID != destructorID) return;
            if (newState)
            {
                ShowDestructibleAreaOnMaps();
                IsSelected = true;
                return;
            }

            IsSelected = false;
        }

        private void ShowDestructibleAreaOnMaps()
        {
            ControlDestructibleMapNull();
            foreach (var mapController in _destructibleMaps)
            {
                var list = GetDestructible(mapController.Map);
                mapController.view.ShowDestructible(list);
            }
        }

        private void ControlDestructibleMapNull()
        {
            if (_destructibleMaps is not null) return;
            _destructibleMaps = new List<DestructibleMapController>();
            var dm = FindObjectsOfType<DestructibleMapController>();
            foreach (var controller in dm)
            {
                if (controller.MapData.isDestructible)
                {
                    _destructibleMaps.Add(controller);
                }
            }
        }


        public bool TryDestruct(DestructibleMap map, Vector2Int point)
        {
            if (!IsDestructible(map, point)) return false;
            var destructedPointList = GetDestructible(map, point);
            Destruct(map, point);
            map.ControlMapScrolling();
            Destructed?.Invoke(destructedPointList);
            IsSelected = false;
            view.Off();
            return true;
        }

        public abstract List<Vector2Int> GetDestructible(Map map);

        protected abstract List<Vector2Int> GetDestructible(Map map,
            Vector2Int point);

        protected abstract void Destruct(Map map, Vector2Int point);
        protected abstract bool IsDestructible(Map map, Vector2Int point);
    }
}