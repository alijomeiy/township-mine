using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Views
{
    [RequireComponent(typeof(Tilemap))]
    // public class MapView : IDLinked<MapView>
    public class MapView : MonoBehaviour
    {
        [SerializeField] private TileIDConfig mapConfig;
        [SerializeField] private Tilemap tilemap;
        [SerializeField] private MapMoverView mapMover;

        private int _mapHeight;

        private void Awake()
        {
            tilemap = GetComponent<Tilemap>();
        }

        public void ShowDestructible(List<Vector2Int> positions)
        {
        }

        public void UpdateMap(string[,] mapContent)
        {
            UpdateMapContent(mapContent);
            var currentMapHeight = mapContent.GetLength(1);
            if (!HaveToMoveMap(currentMapHeight)) return;
            mapMover.GoUp();
            _mapHeight = currentMapHeight;
        }

        private bool HaveToMoveMap(int currentMapHeight)
        {
            return _mapHeight != currentMapHeight;
        }

        private void UpdateMapContent(string[,] mapContent)
        {
            var xCount = mapContent.GetLength(0);
            var yCount = mapContent.GetLength(1);
            for (var i = 0; i < xCount; i++)
            {
                for (var j = 0; j < yCount; j++)
                {
                    var tileName = mapContent[i, j];
                    var tileID = mapConfig.tileIDs.Find(t => t.id == tileName);
                    var position = new Vector3Int(i, j, 0);
                    if (tileID.tile is not null)
                        tilemap.SetTile(position, tileID.tile);
                }
            }
        }

        private static void Log(string[,] content)
        {
            var s = "";
            var xCount = content.GetLength(0);
            var yCount = content.GetLength(1);
            for (var i = 0; i < xCount; i++)
            {
                s += $"\n{i}\n";
                for (var j = 0; j < yCount; j++)
                {
                    var tileName = content[i, j];
                    s += $"\t{j}. ";
                    switch (tileName)
                    {
                        case "":
                            s += "emp";
                            break;
                        case "sand":
                            s += "snd";
                            break;
                        case "stone":
                            s += "stn";
                            break;
                        case "hard stone":
                            s += "hsn";
                            break;
                    }
                }

                Debug.Log("map:" + s);
            }
        }
    }
}