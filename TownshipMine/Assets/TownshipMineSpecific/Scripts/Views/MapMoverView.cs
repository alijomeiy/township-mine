using DG.Tweening;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Views
{
    public class MapMoverView : MonoBehaviour
    {
        [SerializeField] private float verticalUnit;
        private Tween _moveTween;

        public void GoUp()
        {
            _moveTween?.Kill();
            transform.position -= new Vector3(0, verticalUnit, 0);
            _moveTween = transform.DOMove(Vector3.zero, 1);
            _moveTween.SetEase(Ease.InOutSine);
        }
    }
}