using System.Collections.Generic;
using TownshipMineSpecific.Scripts.Models;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Controllers.Interactors
{
    public class HammerController : DestructorController
    {
        public override List<Vector2Int> GetDestructible(Map map)
        {
            return new List<Vector2Int>();
        }

        protected override List<Vector2Int> GetDestructible(Map map,
            Vector2Int point)
        {
            return new List<Vector2Int> {point};
        }

        protected override void Destruct(Map map, Vector2Int point)
        {
            var content = map[point];
            content.RemoveTop();
        }

        protected override bool IsDestructible(Map map, Vector2Int point)
        {
            var content = map[point];
            if (content.IsEmpty()) return false;
            var neighbours = map.GetMain4NeighborContents(point);
            foreach (var neighbour in neighbours)
            {
                if (neighbour.IsEmpty())
                {
                    return true;
                }
            }

            return false;
        }
    }
}