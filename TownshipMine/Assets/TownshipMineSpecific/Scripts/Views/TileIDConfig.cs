using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace TownshipMineSpecific.Scripts.Views
{
    [CreateAssetMenu]
    public class TileIDConfig : ScriptableObject
    {
        public List<TileID> tileIDs;
    }

    [Serializable]
    public class TileID
    {
        public string id;
        public TileBase tile;
    }
}
