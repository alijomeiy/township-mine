using TownshipMineSpecific.Scripts.Misc;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Models
{
    public class Map
    {
        private static readonly Dictionary<string, Map> MapsID = new();
        public string ID { get; }
        public int MapOrder { get; }
        public List<Content[]> Contents { private set; get; }

        public Content this[int x, int y] => Contents[y][x];
        public Content this[Vector2Int pos] => this[pos.x, pos.y];

        private readonly Content[] _notEmptyAcceptableContents;
        private float _emptynessRatio;
        public int MapWidth { get; }
        private readonly int _mapInitializeHeight;
        public int MapHeight => Contents?.Count ?? 0;

        public Map(string id, int width, int height, float emptynessRatio,
            Content[] notEmptyAcceptable, int order)
        {
            ID = id;
            MapOrder = order;
            MapWidth = width;
            _mapInitializeHeight = height;
            _notEmptyAcceptableContents = notEmptyAcceptable;
            SetEmptynessRatio(emptynessRatio);
            MapsID.Add(id, this);
        }

        public static Map Get(string id)
        {
            return MapsID[id];
        }

        public static List<Map> GetHigherMaps(int order)
        {
            var result = new List<Map>();
            foreach (var (id, map) in MapsID)
            {
                if (map.MapOrder > order)
                {
                    result.Add(map);
                }
            }

            return result;
        }

        private void SetEmptynessRatio(float emptynessRatio)
        {
            switch (emptynessRatio)
            {
                case > 1:
                    _emptynessRatio = 1;
                    return;
                case < 0:
                    _emptynessRatio = 0;
                    return;
                default:
                    _emptynessRatio = emptynessRatio;
                    break;
            }
        }

        public Vector2Int GetMapSize()
        {
            return new Vector2Int(MapWidth, MapHeight);
        }

        public virtual void FillRandom()
        {
            FillMapWithAcceptableContents();
            MakeSomeTileEmpty(1, Contents.Count - 1);
        }

        protected void FillMapWithAcceptableContents()
        {
            Contents = new List<Content[]>();
            for (var j = 0; j < _mapInitializeHeight; j++)
            {
                AddRandomRowBottom();
            }
        }

        protected void MakeSomeTileEmpty(int startRow, int endRowInclusive)
        {
            for (var i = startRow; i <= endRowInclusive; i++)
            {
                var contents = Contents[i];
                foreach (var content in contents)
                {
                    if (ShouldBasedEmptynessRatioBeEmpty())
                    {
                        content.MakeEmpty();
                    }
                }
            }
        }

        private bool ShouldBasedEmptynessRatioBeEmpty()
        {
            var random = Random.Range(0, 100);
            var r = _emptynessRatio * 100;
            return random < r;
        }

        public string[,] GetMap()
        {
            var result = new string[MapWidth, MapHeight];
            for (var i = 0; i < MapWidth; i++)
            {
                for (var j = 0; j < MapHeight; j++)
                {
                    result[i, j] = this[i, j].GetTop();
                }
            }

            return result;
        }

        public List<Content> GetMain4NeighborContents(Vector2Int position)
        {
            var size = GetMapSize();
            var positions = MapUtilities.Get4MainNeighbourIndex(position, size);
            var result = new List<Content>(positions.Count);
            foreach (var pos in positions)
            {
                result.Add(this[pos.x, pos.y]);
            }

            return result;
        }

        public List<Content> GetAllNeighborContents(Vector2Int position)
        {
            var size = GetMapSize();
            var positions = MapUtilities.GetNeighboursIndex(position, size);
            var result = new List<Content>(positions.Count);
            foreach (var pos in positions)
            {
                result.Add(this[pos.x, pos.y]);
            }

            return result;
        }

        protected virtual void AddRandomRowBottom()
        {
            var row = new Content[MapWidth];
            for (var i = 0; i < MapWidth; i++)
            {
                var randomIndex =
                    Random.Range(0, _notEmptyAcceptableContents.Length);
                row[i] = _notEmptyAcceptableContents[randomIndex].Clone();
            }

            Contents.Insert(0, row);
        }
    }
}