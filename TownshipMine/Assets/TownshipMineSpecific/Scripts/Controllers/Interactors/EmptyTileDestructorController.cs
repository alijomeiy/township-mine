using TownshipMineSpecific.Scripts.Models;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Controllers.Interactors
{
    public abstract class EmptyTileDestructorController : DestructorController
    {
        protected override bool IsDestructible(Map map, Vector2Int point)
        {
            var content = map[point.x, point.y];
            return content.IsEmpty();
        }
    }
}