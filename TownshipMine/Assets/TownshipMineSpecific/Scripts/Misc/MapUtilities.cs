using System.Collections.Generic;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Misc
{
    public static class MapUtilities
    {
        private static readonly List<Vector2Int>
            MainNeighbours = new()
            {
                Vector2Int.up,
                Vector2Int.down,
                Vector2Int.left,
                Vector2Int.right
            };

        private static readonly List<Vector2Int>
            AllNeighbours = new(MainNeighbours)
            {
                Vector2Int.up + Vector2Int.left,
                Vector2Int.up + Vector2Int.right,
                Vector2Int.down + Vector2Int.left,
                Vector2Int.down + Vector2Int.right,
            };

        public static List<Vector2Int> Get4MainNeighbourIndex(Vector2Int main,
            Vector2Int mapSize)
        {
            return FilterTrueNeighbour(main, mapSize, MainNeighbours);
        }

        public static List<Vector2Int> GetNeighboursIndex(Vector2Int main,
            Vector2Int mapSize)
        {
            return FilterTrueNeighbour(main, mapSize, AllNeighbours);
        }

        private static List<Vector2Int> FilterTrueNeighbour(Vector2Int main,
            Vector2Int mapSize, IEnumerable<Vector2Int> neighbours)
        {
            var result = new List<Vector2Int>(neighbours);
            var i = 0;
            while (i < result.Count)
            {
                var newValue = result[i] + main;
                if (IsTrueMapValue(newValue, mapSize))
                {
                    result[i] = newValue;
                    i++;
                }
                else
                {
                    result.RemoveAt(i);
                }
            }

            return result;
        }

        private static bool IsTrueMapValue(Vector2Int newValue,
            Vector2Int mapSize)
        {
            var trueX = newValue.x >= 0 && newValue.x < mapSize.x;
            var trueY = newValue.y >= 0 && newValue.y < mapSize.y;
            return trueX && trueY;
        }
    }
}