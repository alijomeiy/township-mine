using System;
using TownshipMineSpecific.Scripts.Views;
using UnityEngine.Tilemaps;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Controllers.Interactors
{
    public abstract class InteractorController : MonoBehaviour
    {
        public static Action Interacted;
        [SerializeField] private string userInputID;

        private void OnEnable()
        {
            UserInput.TouchEnded += HandleTouchEnd;
            SubscribeEvents();
        }

        private void OnDisable()
        {
            UserInput.TouchEnded -= HandleTouchEnd;
            UnsubscribeEvents();
        }

        private void HandleTouchEnd(string mapID, TileBase tile,
            Vector3Int tileMapPosition)
        {
            if (!IsInteractable(mapID, tile, tileMapPosition)) return;
            Interact(tile, tileMapPosition);
            Interacted?.Invoke();
        }

        protected virtual bool IsInteractable(string mapID, TileBase tile,
            Vector3Int pos)
        {
            return mapID == userInputID;
        }

        protected abstract void Interact(TileBase tile, Vector3Int position);

        protected virtual void UnsubscribeEvents()
        {
        }

        protected virtual void SubscribeEvents()
        {
        }
    }
}