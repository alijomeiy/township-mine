using System.Collections.Generic;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Models
{
    [CreateAssetMenu]
    public class MapData : ScriptableObject
    {
        public string id;
        public List<TileContent> contents;
        public TileContent emptyTile;
        public Vector2Int mapSize;
        public bool isDestructible;
        public float emptynessRatio;
        public int order;
        public int Weight => mapSize.x;
        public int Height => mapSize.y;
    }
}