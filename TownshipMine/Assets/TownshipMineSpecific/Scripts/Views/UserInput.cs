using System;
using NaughtyAttributes;
using UnityEngine.Tilemaps;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Views
{
    [RequireComponent(typeof(Tilemap))]
    public class UserInput : MonoBehaviour
    {
        public static Action<string, TileBase, Vector3Int> TouchBegined;
        public static Action<string, TileBase, Vector3Int> TouchMoved;
        public static Action<string, TileBase, Vector3Int> TouchEnded;
        [SerializeField, Label("ID")] private string id;
        private Tilemap _tilemap;

        private void Awake()
        {
            _tilemap = GetComponent<Tilemap>();
        }

        private void Update()
        {
            ControlTouch();
        }

        private void ControlTouch()
        {
            if (!IsTouched()) return;
            if (!TryGetTouchedTile(out var tile, out var position)) return;
            if (Application.isEditor)
            {
                FireEditorEvent(tile, position);
                return;
            }

            FireTouchEvent(tile, position);
        }

        private void FireEditorEvent(TileBase tile, Vector3Int position)
        {
            if (Input.GetMouseButtonDown(0))
            {
                TouchBegined?.Invoke(id, tile, position);
                TouchEnded?.Invoke(id, tile, position);
            }

            if (Input.GetMouseButtonUp(0))
            {
                TouchEnded?.Invoke(id, tile, position);
            }
        }

        private void FireTouchEvent(TileBase tile, Vector3Int position)
        {
            switch (Input.touches[0].phase)
            {
                case TouchPhase.Began:
                    TouchBegined?.Invoke(id, tile, position);
                    break;
                case TouchPhase.Moved:
                    TouchMoved?.Invoke(id, tile, position);
                    break;
                case TouchPhase.Canceled:
                case TouchPhase.Ended:
                    TouchEnded?.Invoke(id, tile, position);
                    break;
            }
        }

        private static bool IsTouched()
        {
            var mouse = Input.GetMouseButtonDown(0);
            var touch = Input.touchCount != 0;
            return Application.isEditor ? mouse : touch;
        }

        private bool TryGetTouchedTile(out TileBase tile,
            out Vector3Int position)
        {
            var touchedPosition = GetTouchedPosition();
            touchedPosition.z = -Camera.main.transform.position.z;
            var worldPoint = Camera.main.ScreenToWorldPoint(touchedPosition);
            position = _tilemap.WorldToCell(worldPoint);
            tile = _tilemap.GetTile(position);
            return tile is not null;
        }

        private static Vector3 GetTouchedPosition()
        {
            return Application.isEditor
                ? Input.mousePosition
                : Input.touches[0].position;
        }
    }
}