using TownshipMineSpecific.Scripts.Models;

namespace TownshipMineSpecific.Scripts.Controllers
{
    public class PreciousMapController : MapController<PreciousMap>
    {
        protected override void GenerateMap()
        {
            var contents = GenerateContent();
            Map = new PreciousMap(mapData.id, mapData.Weight,
                mapData.Height, mapData.emptynessRatio, contents,
                mapData.order);
            Map.FillRandom();
        }

        protected override void Subscribe()
        {
            base.Subscribe();
            DestructibleMap.MapMoved += HandleMapMove;
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            DestructibleMap.MapMoved -= HandleMapMove;
        }

        private void HandleMapMove(string mapID)
        {
            Map.AddNewRow();
            var content = Map.GetMap();
            view.UpdateMap(content);
        }
    }
}