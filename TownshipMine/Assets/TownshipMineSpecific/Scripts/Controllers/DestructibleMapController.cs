using TownshipMineSpecific.Scripts.Models;

namespace TownshipMineSpecific.Scripts.Controllers
{
    public class DestructibleMapController : MapController<DestructibleMap>
    {
        protected override void GenerateMap()
        {
            var contents = GenerateContent();
            Map = new DestructibleMap(mapData.id, mapData.Weight,
                mapData.Height, mapData.emptynessRatio, contents,
                mapData.order);
            Map.FillRandom();
        }
    }
}