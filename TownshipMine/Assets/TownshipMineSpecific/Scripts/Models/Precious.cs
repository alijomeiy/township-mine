using System.Collections.Generic;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Models
{
    public class Precious
    {
        private static readonly Dictionary<string, Precious> Treasures = new();

        public Precious(string id)
        {
            if (Treasures.ContainsKey(id)) return;
            ID = id;
            Treasures.Add(ID, this);
            Value = PlayerPrefs.GetInt(id, 0);
        }

        public string ID { get; }
        public int Value { get; private set; }

        public void Add(int addedValue)
        {
            Value += addedValue;
            PlayerPrefs.SetInt(ID, Value);
        }

        public static Precious Get(string id)
        {
            return !Treasures.ContainsKey(id) ? null : Treasures[id];
        }
    }
}