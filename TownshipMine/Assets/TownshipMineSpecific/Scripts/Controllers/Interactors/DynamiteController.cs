using System.Collections.Generic;
using TownshipMineSpecific.Scripts.Models;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Controllers.Interactors
{
    public class DynamiteController : EmptyTileMultipleDestructorController
    {
        protected override List<Vector2Int> GetUnderDestructionTiles(
            Vector2Int point, Vector2Int mapSize)
        {
            var count = mapSize.x;
            var result = new List<Vector2Int>(count);
            for (var i = 0; i < count; i++)
            {
                result.Add(new Vector2Int(i, point.y));
            }

            return result;
        }

        protected override void ExecuteDestructionMethod(Content content)
        {
            content.MakeEmpty();
        }
    }
}