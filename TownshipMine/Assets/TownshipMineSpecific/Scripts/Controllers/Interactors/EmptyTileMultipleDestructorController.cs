using System.Collections.Generic;
using TownshipMineSpecific.Scripts.Models;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Controllers.Interactors
{
    public abstract class
        EmptyTileMultipleDestructorController : EmptyTileDestructorController
    {
        public override List<Vector2Int> GetDestructible(Map map)
        {
            return new List<Vector2Int>();
        }

        protected override List<Vector2Int> GetDestructible(Map map,
            Vector2Int point)
        {
            var size = map.GetMapSize();
            return GetUnderDestructionTiles(point, size);
        }

        protected override void Destruct(Map map, Vector2Int point)
        {
            var size = map.GetMapSize();
            var list = GetUnderDestructionTiles(point, size);
            foreach (var p in list)
            {
                var content = map[p.x, p.y];
                ExecuteDestructionMethod(content);
            }
        }

        protected virtual void ExecuteDestructionMethod(Content content)
        {
            content.RemoveTop();
        }

        protected abstract List<Vector2Int> GetUnderDestructionTiles(
            Vector2Int point, Vector2Int mapSize);
    }
}