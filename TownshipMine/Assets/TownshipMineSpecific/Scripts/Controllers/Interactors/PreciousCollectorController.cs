using System;
using TownshipMineSpecific.Scripts.Models;
using UnityEngine.Tilemaps;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Controllers.Interactors
{
    public class PreciousCollectorController : InteractorController
    {
        public static Action<string, int> Collected;
        [SerializeField] private PreciousMapController preciousMapController;

        protected override void Interact(TileBase tile, Vector3Int position)
        {
            var c = preciousMapController.Map[position.x, position.y];
            c.RemoveTop();
            Collected?.Invoke(tile.name, 1);
        }

        protected override bool IsInteractable(string mapID, TileBase tile,
            Vector3Int pos)
        {
            var baseResult = base.IsInteractable(mapID, tile, pos);
            var c = preciousMapController.Map[pos.x, pos.y];
            return baseResult && !c.IsEmpty() && IsUpperLayerEmpty(pos);
        }

        private bool IsUpperLayerEmpty(Vector3Int pos)
        {
            var order = preciousMapController.Map.MapOrder;
            var maps = Map.GetHigherMaps(order);
            foreach (var map in maps)
            {
                var mapContent = map[pos.x, pos.y];
                if (!mapContent.IsEmpty())
                {
                    return false;
                }
            }

            return true;
        }
    }
}