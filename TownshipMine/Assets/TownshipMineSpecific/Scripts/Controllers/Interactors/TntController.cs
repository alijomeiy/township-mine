using System.Collections.Generic;
using TownshipMineSpecific.Scripts.Misc;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Controllers.Interactors
{
    public class TntController : EmptyTileMultipleDestructorController
    {
        protected override List<Vector2Int> GetUnderDestructionTiles(
            Vector2Int point, Vector2Int mapSize)
        {
            return MapUtilities.GetNeighboursIndex(point, mapSize);
        }
    }
}