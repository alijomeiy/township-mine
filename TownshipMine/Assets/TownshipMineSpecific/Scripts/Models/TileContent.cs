using UnityEngine;

namespace TownshipMineSpecific.Scripts.Models
{
    [CreateAssetMenu]
    public class TileContent : ScriptableObject
    {
        [SerializeField] private string[] ordered;

        public string[] Get()
        {
            return ordered;
        }
    }
}