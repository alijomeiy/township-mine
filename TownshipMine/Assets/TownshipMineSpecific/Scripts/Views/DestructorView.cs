using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TownshipMineSpecific.Scripts.Views
{
    public class DestructorView : MonoBehaviour
    {
        private static readonly List<DestructorView> SelectedViews = new();
        private static bool IsNothingSelected => SelectedViews.Count == 0;
        private static string FirstID => SelectedViews[0].id;

        public static string SelectedID => IsNothingSelected ? "" : FirstID;

        [SerializeField] private string id;
        [SerializeField] private Toggle toggle;

        public static Action<string, bool> SelectionStateChanged;

        public void ChangeSelected(bool newState)
        {
            if (newState && !SelectedViews.Contains(this))
            {
                SelectedViews.Add(this);
            }
            else if (!newState)
            {
                SelectedViews.Remove(this);
            }

            SelectionStateChanged?.Invoke(id, newState);
        }

        public void Off()
        {
            toggle.isOn = false;
        }
    }
}