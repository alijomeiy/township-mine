using System;

namespace TownshipMineSpecific.Scripts.Models
{
    public class DestructibleMap : Map
    {
        public static Action<string> MapMoved;

        public DestructibleMap(string id, int width, int height,
            float emptynessRatio, Content[] notEmptyAcceptable, int order) :
            base(id, width, height, emptynessRatio, notEmptyAcceptable, order)
        {
        }

        public void ControlMapScrolling()
        {
            if (!IsBottomRowHaveFreePlace()) return;
            AddRandomRowBottom();
            FireMoveEvent();
        }

        private bool IsBottomRowHaveFreePlace()
        {
            for (var i = 0; i < MapWidth; i++)
            {
                if (this[i, 0].IsEmpty())
                {
                    return true;
                }
            }

            return false;
        }

        private void FireMoveEvent()
        {
            MapMoved?.Invoke(ID);
        }
    }
}