using System.Collections.Generic;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Models
{
    public class Content
    {
        private List<string> _orderedContent;

        public Content(string[] orderedArray)
        {
            _orderedContent = new List<string>(orderedArray);
        }

        public string GetTop()
        {
            var count = _orderedContent.Count;
            return count > 0 ? _orderedContent[count - 1] : "";
        }

        public void RemoveTop()
        {
            var count = _orderedContent.Count;
            if (count > 0)
            {
                _orderedContent.RemoveAt(count - 1);
            }
        }

        public void MakeEmpty()
        {
            _orderedContent = new List<string>();
        }

        public bool IsEmpty()
        {
            return GetTop() == "";
        }

        public Content Clone()
        {
            return new Content(_orderedContent.ToArray());
        }
    }
}