using TownshipMineSpecific.Scripts.Controllers.Interactors;
using TownshipMineSpecific.Scripts.Models;
using TownshipMineSpecific.Scripts.Views;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Controllers
{
    public abstract class MapController<T> : MonoBehaviour where T : Map
    {
        [SerializeField] protected MapData mapData;

        public MapData MapData => mapData;
        public T Map { get; protected set; }

        public MapView view;

        private void Awake()
        {
            GenerateMap();
            UpdateMapView();
        }

        private void OnEnable()
        {
            InteractorController.Interacted += HandleInteraction;
            Subscribe();
        }


        private void OnDisable()
        {
            InteractorController.Interacted -= HandleInteraction;
            Unsubscribe();
        }

        protected virtual void Subscribe()
        {
        }

        protected virtual void Unsubscribe()
        {
        }

        private void HandleInteraction()
        {
            UpdateMapView();
        }

        protected abstract void GenerateMap();

        protected void UpdateMapView()
        {
            var map = Map.GetMap();
            view.UpdateMap(map);
        }

        protected Content[] GenerateContent()
        {
            var contents = mapData.contents;
            var count = contents.Count;
            var contentList = new List<Content>(count);
            foreach (var content in contents)
            {
                var c = new Content(content.Get());
                contentList.Add(c);
            }

            return contentList.ToArray();
        }
    }
}