using System;
using TownshipMineSpecific.Scripts.Controllers.Interactors;
using TownshipMineSpecific.Scripts.Models;
using TownshipMineSpecific.Scripts.Views;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

namespace TownshipMineSpecific.Scripts.Controllers
{
    public class PreciousController : MonoBehaviour
    {
        [SerializeField, Label("IDs")] private List<string> ids;
        [SerializeField] private PreciousMapController preciousMapController;

        private void Awake()
        {
            GeneratePrecious();
        }

        private void Start()
        {
            UpdateViews();
        }

        private void OnEnable()
        {
            DestructibleMap.MapMoved += HandleDestructibleMapMove;
            PreciousCollectorController.Collected += HandleCollection;
        }

        private void OnDisable()
        {
            DestructibleMap.MapMoved -= HandleDestructibleMapMove;
            PreciousCollectorController.Collected -= HandleCollection;
        }

        private void UpdateView(string id)
        {
            var treasure = Precious.Get(id);
            var newCount = treasure.Value;
            var treasureView = PreciousView.Get(id);
            treasureView.CurrentText = newCount.ToString();
        }

        private void UpdateViews()
        {
            foreach (var id in ids)
            {
                UpdateView(id);
            }
        }

        private void HandleDestructibleMapMove(string mapID)
        {
            var map = preciousMapController.Map;
        }

        private void HandleCollection(string collectedName, int count)
        {
            var t = Precious.Get(collectedName);
            var tv = PreciousView.Get(collectedName);
            t.Add(count);
            tv.CurrentText = t.Value.ToString();
        }

        private void GeneratePrecious()
        {
            foreach (var id in ids)
            {
                var treasure = new Precious(id);
            }
        }
    }
}