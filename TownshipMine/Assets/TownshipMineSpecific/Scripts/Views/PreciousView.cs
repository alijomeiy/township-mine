using System;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using TMPro;

namespace TownshipMineSpecific.Scripts.Views
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class PreciousView : MonoBehaviour
    {
        private static readonly Dictionary<string, PreciousView> Treasures =
            new();

        [SerializeField, Label("ID")] private string id;
        public string ID => id;
        private TextMeshProUGUI _countText;

        public string CurrentText
        {
            get => _countText.text;
            set => _countText.text = value;
        }

        public static PreciousView Get(string treasureID)
        {
            return Treasures[treasureID];
        }

        private void Awake()
        {
            _countText = GetComponent<TextMeshProUGUI>();
            Treasures.Add(id, this);
        }

        private void OnDestroy()
        {
            Treasures.Remove(id);
        }
    }
}